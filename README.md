# feed-consumer

This is a prototype app used as a consumer for Odds Feed. The purpose of the app is to walk you through the process of connecting to your [RabbitMQ](https://www.rabbitmq.com/) node. To help you understand the different concepts of messages received on different queues and to provide a better view of different schema status and their meaning.

## Prerequisites

In order to run this app you need to have [Node.js](https://nodejs.org/en) installed. There is no specific version required.

## Environment

Before you start the installation you need to set up your enviromental variables.

| Variable       | Usage                                                                                                                                |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| RMQ_URI        | Define RMQ URI to connect to your node. Example `amqp://rmquser:rmqpassword&node_id:node_port/`                                      |
| PERSISTER_TYPE | Defines different cache types. _Possible values: internal, [redis](https://redis.io/), db ([postgres](https://www.postgresql.org/))_ |

## Quick Start

To install all dependencies run:

```sh
npm install
```

Start the app:

```sh
npm start
```
