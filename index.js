require('dotenv').config();
const App = require('./src/app');

async function init() {
	try {
		const app = new App();
		await app.start();
	} catch (error) {
		console.log(error);
	}
}

init();
