const PersistFactory = require('./modules/persisters/persister-factory');
const ConsumerFactory = require('./modules/consummers/consumer-factory');

class App {
	constructor() {}

	async start() {
		this.persister = await PersistFactory.create();

		this.fixturesConsumer = await ConsumerFactory.createConsumer(this.persister, 'fixtures');
		this.statisticsConsumer = await ConsumerFactory.createConsumer(this.persister, 'statistics');
		this.signalsConsumer = await ConsumerFactory.createConsumer(this.persister, 'signals');
		this.settlementConsumer = await ConsumerFactory.createConsumer(this.persister, 'settlement');
		await this.fixturesConsumer.startConsuming();
		await this.statisticsConsumer.startConsuming();
		await this.signalsConsumer.startConsuming();
		await this.settlementConsumer.startConsuming();
	}
}

module.exports = App;
