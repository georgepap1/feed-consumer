module.exports = {
	consumeQueues: {
		fixtures: 'client-fixtures-queue',
		statistics: 'client-statistics-queue',
		signals: 'client-signals-queue',
		settlement: 'client-settlements-queue'
	},
	signalTypes: {
		stopConsuming: 'stop-consuming',
		startConsuming: 'start-consuming'
	},
	tables: {
		FIXTURES: 'fixtures',
		MARKETS: 'markets',
		ODDS: 'odds',
		STATISTICS: 'statistics',
		SETTLEMENT: 'settlement',
		LIVE: 'live'
	},
	fixtureStatuses: {
		IN_PROGRESS: 'In progress',
		FINISHED: 'Finished',
		PRE_FINISHED: 'PreFinished',
		COVERAGE_LOST: 'Coverage lost',
		SUSPENDED: 'Suspended'
	},
	marketOddStatuses: {
		NEW: 'new',
		UPDATED: 'updated',
		DELETED: 'deleted',
		SUSPENDED: 'suspended',
		SETTLED: 'settled'
	}
};
