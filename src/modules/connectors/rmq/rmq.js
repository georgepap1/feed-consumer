const amqp = require('amqplib');
const Helpers = require('../../utils/helpers');

class RmqConnection {
	constructor(messageHandler, uri, inputQueue) {
		this.messageHandler = messageHandler;
		this.inputQueue = inputQueue;
		this.amqUrl = uri;
	}

	async init() {
		return await amqp
			.connect(this.amqUrl)
			.then(async (connection) => {
				console.log('RMQ connected');
				this.connection = connection;
				this.connection.on('error', async (err) => {
					console.error(`Connection Error:`, err.message);
					await this.reConnect();
				});
				this.connection.on('close', async (err) => {
					console.error(`Connection Close:`, err.message);
					await this.reConnect();
				});
				this.channel = await connection.createChannel();
				await this.channel.prefetch(1);
				this.consumeMessage();
			})
			.catch((error) => {
				console.error('RMQ Connection error: ', error);
				this.reConnect();
			});
	}

	async reConnect() {
		this.channel = null;
		this.connection = null;
		console.log(`Attempting Reconnection!`);
		await Helpers.makeTimeout(1000);
		await this.init();
	}

	consumeMessage() {
		this.channel.consume(
			this.inputQueue,
			this.handle.bind(this), //handler for the message
			{ noAck: true }
		);
	}

	async handle(message) {
		try {
			await this.messageHandler.handle(message);
		} catch (error) {
			await this.reConnect();
			console.log(error);
		}
	}
}

module.exports = RmqConnection;
