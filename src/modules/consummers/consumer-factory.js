const constants = require('../../constants');

class ConsumerFactory {
	constructor() {}

	static async createConsumer(persister, consumerType) {
		const Handler = require(`../handlers/${consumerType}-handler`);
		const handler = new Handler(persister);
		const Consumer = require('./consumer');
		const consumer = new Consumer(handler, constants.consumeQueues[consumerType]);
		return consumer;
	}
}

module.exports = ConsumerFactory;
