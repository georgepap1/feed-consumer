const Rmq = require('../connectors/rmq/rmq');

class Consummer {
	constructor(handler, queue) {
		this.handler = handler;
		this.rmq = new Rmq(this.handler, process.env.RMQ_URI, queue);
	}

	async startConsuming() {
		await this.rmq.init();
	}
}

module.exports = Consummer;
