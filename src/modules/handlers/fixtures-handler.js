const Handler = require('./handler');
const constants = require('../../constants');

class FixturesHandler extends Handler {
	constructor(persister) {
		super(persister);
	}

	async handle(message) {
		super.handle(message);
		console.log('Handling Fixture:', this.message);
		const fixture = this.message.body.fixture;
		const fixtureStatus = fixture.status;

		if ([constants.fixtureStatuses.FINISHED, constants.fixtureStatuses.PRE_FINISHED, constants.fixtureStatuses.SUSPENDED, constants.fixtureStatuses.COVERAGE_LOST].includes(fixtureStatus)) {
			await this.persister.deleteElement(constants.tables.FIXTURES, fixture.fixtureId);
			// Also you must delete markets and odds for this fixture
			// if you are using relational DB to persist your data you can use referential integrity (cascade delete) to delete all of them
		} else {
			await this.persister.setElement(fixture, constants.tables.FIXTURES, fixture.fixtureId);
		}

		this.handleMarkets(this.message.body.markets);
	}

	async handleMarkets(markets) {
		for (const market of markets) {
			if (constants.marketOddStatuses.DELETED === markets.status) {
				await this.persister.deleteElement(constants.tables.MARKETS, market.marketId);
				// Also you must delete the odds for this market
				// if you are using relational DB to persist your data you can use referential integrity (cascade delete) to delete all of them
			} else if (constants.marketOddStatuses.SUSPENDED == market.status) {
				// You must suspend the market
			} else if ([constants.marketOddStatuses.NEW, constants.marketOddStatuses.UPDATED].includes(market.status)) {
				await this.persister.setElement(market, constants.tables.MARKETS, market.marketId);
			}

			this.handleOdds(market.odds);
		}
	}

	async handleOdds(odds) {
		for (const odd of odds) {
			if (constants.marketOddStatuses.DELETED === odd.status) {
				await this.persister.deleteElement(constants.tables.ODDS, odd.properties.uuid);
			} else if (constants.marketOddStatuses.SUSPENDED === odd.status) {
				// You must suspend the odd
			} else if ([constants.marketOddStatuses.NEW, constants.marketOddStatuses.UPDATED].includes(odd.status)) {
				await this.persister.setElement(odd, constants.tables.ODDS, odd.properties.uuid);
			}
		}
	}
}

module.exports = FixturesHandler;
