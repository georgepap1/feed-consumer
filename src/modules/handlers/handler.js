class Handler {
	constructor(persister) {
		this.persister = persister;
	}

	async handle(message) {
		this.message = JSON.parse(message.content.toString());
	}
}

module.exports = Handler;
