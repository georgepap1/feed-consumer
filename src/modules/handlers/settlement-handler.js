const Handler = require('./handler');
const constants = require('../../constants');

class SettlementHandler extends Handler {
	constructor(persister) {
		super(persister);
	}

	async handle(message) {
		super.handle(message);
		console.log('Handling Settlement:', this.message);
		const markets = this.message.body.markets;
		for (const market of markets) {
			const odds = market.odds;
			for (const odd of odds) {
				if (odd.status === constants.marketOddStatuses.SETTLED) {
					// Add your logic here to handle a settled Odd
					this.persister.setElement(odd, constants.tables.SETTLEMENT, odd.properties.uuid);
				}
			}
		}
	}
}

module.exports = SettlementHandler;
