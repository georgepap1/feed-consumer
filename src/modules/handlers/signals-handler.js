const Handler = require('./handler');
const constants = require('../../constants');

class StatsHandler extends Handler {
	constructor(persister) {
		super(persister);
	}

	async handle(message) {
		super.handle(message);
		const signal = this.message;
		console.log('Handling Signal:', signal);
		switch (signal) {
			case constants.signalTypes.stopConsuming:
				this.handleStartSignal();
				break;
			case constants.signalTypes.startConsuming:
				this.handleStopSignal();
				break;
			default:
				console.log(`Not implemented signal received: ${signal}`);
		}
	}

	handleStartSignal() {
		// You can start consuming our messages.
		// We highly recommend you delete your cache once you receive this signal and request and update
		return;
	}

	handleStopSignal() {
		// You should disregard all messages we sent you from now on until you receive as start-consuming message.
		// We use this message to inform you for possible maintenance or technical issue we are facing.
		// We highly recommend you disable your entiry sportsbook
		return;
	}
}

module.exports = StatsHandler;
