const Handler = require('./handler');
const constants = require('../../constants');

class StatsHandler extends Handler {
	constructor(persister) {
		super(persister);
	}

	async handle(message) {
		super.handle(message);
		const liveData = this.message.body.live;
		const statsData = this.message.body.stats;
		this.handleLiveData(liveData);
		this.handleStatsData(statsData);
	}

	handleLiveData(liveData) {
		if (liveData == null) return;
		console.log('handling Live Data:', liveData);
		this.persister.setElement(liveData, constants.tables.LIVE, this.message.body.fixture.fixtureId);
	}

	handleStatsData(statsData) {
		if (statsData == null) return;
		console.log('handling Live stats:', statsData);
		this.persister.setElement(statsData, constants.tables.STATISTICS, this.message.body.fixture.fixtureId);
	}
}

module.exports = StatsHandler;
