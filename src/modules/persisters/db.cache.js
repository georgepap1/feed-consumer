const db = require('../../connectors/pg');

const cache = [];

class Cache {
	constructor() {
		super();
		console.log('DB cache initializing');
	}

	init() {}

	async getTableElementById(id, table) {
		const query = {
			text: `SELECT * FROM "${process.env.PGSCHEMA}"."${table}" where "id"=$1`,
			values: [id]
		};

		const { rows } = await db.query(query);

		return rows;
	}

	async setTableElement(element, table) {
		let query = {
			text: `INSERT INTO "${process.env.PGSCHEMA}"."${table}"	(${element.columns}) VALUES (`,
			values: []
		};

		for (let i = 0; i < element.data.length; i++) {
			query.text = query.text + ' $' + (i + 1) + ',';
			query.values.push(element.data[i]);
		}
		query.text = query.text.slice(0, -1) + ')';

		return await db
			.query(query)
			.then((result) => {
				return result;
			})
			.catch((error) => {
				console.error(error, query);
				return { rows: null };
			});
	}

	async getUniElement(element, table) {
		let query = {
			text: `select ${element.columns} from "${process.env.PGUNISCHEMA}"."${table}" where `,
			values: []
		};

		for (let i = 0; i < element.data.length; i++) {
			query.text = query.text + ' "' + element.data[i].column + '" = $' + (i + 1) + ' and ';
			query.values.push(element.data[i].value);
		}
		query.text = query.text.slice(0, -5);

		return await db
			.query(query)
			.then((result) => {
				return result;
			})
			.catch((error) => {
				console.error(error, query);
				return { rows: null };
			});
	}

	async setUniTableElement(element, table) {
		let query = {
			text: `INSERT INTO "${process.env.PGUNISCHEMA}"."${table}"	(${element.columns}) VALUES (`,
			values: []
		};

		for (let i = 0; i < element.data.length; i++) {
			query.text = query.text + ' $' + (i + 1) + ',';
			query.values.push(element.data[i]);
		}
		query.text = query.text.slice(0, -1) + ')  RETURNING id';

		return db
			.query(query)
			.then((result) => {
				return result;
			})
			.catch((error) => {
				console.error(error, query);
				return { rows: null };
			});
	}

	async getAll(table) {
		const query = `SELECT * FROM "${process.env.PGSCHEMA}"."${table}"`;
		const { rows } = await db.query(query);

		return rows;
	}
}

module.exports = Cache;
