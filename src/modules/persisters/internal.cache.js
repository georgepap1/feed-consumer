const Persister = require('./persister');

const cache = { fixtures: {}, markets: {}, odds: {}, live: {}, statistics: {}, settlement: {} };

class MemoryPersister extends Persister {
	constructor() {
		super();
		console.log('internal memory persister instantiating');
	}

	init() {}

	elementExists(table, id) {
		if (cache[table][id]) return true;
		return false;
	}

	deleteElement(table, id) {
		delete cache[table][id];
	}

	getElementById(table, id) {
		return cache[table][id];
	}

	setElement(element, table, id) {
		cache[table][id] = element;
		return element;
	}

	getAll(table) {
		return cache[table];
	}
}

module.exports = MemoryPersister;
