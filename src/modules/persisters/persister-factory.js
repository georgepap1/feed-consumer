class PersisterFactory {
	constructor() {}

	static async create(type = null) {
		const persisterType = type ? type : process.env.PERSISTER_TYPE ? process.env.PERSISTER_TYPE : 'internal';
		const Persister = require(`./${persisterType}.cache.js`);
		const persister = new Persister();
		await persister.init();
		return persister;
	}
}

module.exports = PersisterFactory;
