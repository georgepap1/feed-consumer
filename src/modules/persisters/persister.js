class Persister {
	constructor() {
		console.log('cache initializing');
	}

	async init() {}

	async elementExists(id, table) {}

	async getElementById(id, table) {}

	async setElement(element, table) {}

	async deleteElement(id, table) {}

	async getAll(table) {}
}

module.exports = Persister;
