'use strict';

const Redis = require('ioredis');
const Persister = require('./persister');
const client = new Redis();

class Cache extends Persister {
	constructor() {
		super();
		console.log('Redis cache instantiating');
	}

	async init() {
		//console.log('Redis connecting ' + process.env.REDIS_URL);
		return true;
	}

	async elementExists(table, id) {
		if (await client.hexists(table, id)) return true;
		return false;
	}

	async getElementById(table, id) {
		return await client.hget(table, id);
	}

	async setElement(element, table, id) {
		await client.hset(table, id, JSON.stringify(element));
	}

	async setTableElement(element, table, id) {
		await client
			.hset(table, id, element, 0)
			.then((result) => {
				return result;
			})
			.catch((error) => {
				console.error(error, query);
			});
	}

	async deleteElement(table, id) {
		await client.hdel(table, id);
	}

	async getAll(table) {
		return await client
			.hgetall(table)
			.then((result) => {
				return result;
			})
			.catch((error) => {
				console.error(error, query);
			});
	}
}

module.exports = Cache;
