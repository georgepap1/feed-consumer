module.exports = class Helpers {
	static makeTimeout(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}
};
